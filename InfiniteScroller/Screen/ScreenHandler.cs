﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace InfiniteScroller.Screen
{
    internal class ScreenHandler
    {
        private static ContentManager Content;

        public ScreenHandler(ContentManager content)
        {
            Content = content;
        }

        private static List<GameScreen> screens = new List<GameScreen>();
        private static List<GameScreen> toBeAdded = new List<GameScreen>();
        private static List<GameScreen> toBeRemoved = new List<GameScreen>();

        public static void AddScreen(GameScreen screen)
        {
            toBeAdded.Add(screen);
            screen.LoadContent(Content);
        }

        public static void RemoveScreen(GameScreen screen)
        {
            screens.Remove(screen);
            screen.UnloadContent();
        }

        public static void Update(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.state == ScreenState.Active || screen.state == ScreenState.Hidden)
                {
                    screen.Update(gameTime);
                }
                else if (screen.state == ScreenState.Off)
                {
                    toBeRemoved.Add(screen);
                }
            }

            foreach (GameScreen screen in toBeAdded)
            {
                screens.Add(screen);
            }

            toBeAdded.Clear();

            foreach (GameScreen screen in toBeRemoved)
            {
                screens.Remove(screen);
            }

            toBeRemoved.Clear();
        }

        public static void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.state == ScreenState.Active || screen.state == ScreenState.Visible)
                {
                    screen.Draw(gameTime);
                }
            }
        }
    }
}