﻿using InfiniteScroller.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace InfiniteScroller.Screen
{
    /// <summary>
    /// Represents the current state of the GameScreen
    /// Off means the screen is completely off and should be unloaded and removed
    /// Active means the screen should both update and draw
    /// Visible means the screen should only draw
    /// Hidden means the screen should only update
    /// Paused means the screen should do nothing, but stay loaded
    /// </summary>
    public enum ScreenState
    {
        Off,
        Active,
        Visible,
        Hidden,
        Paused
    }

    /// <summary>
    /// Represents a screen in a game
    /// </summary>
    public class GameScreen
    {
        /// <summary>
        /// The current state of the screen
        /// </summary>
        public ScreenState state;

        protected SpriteBatch spriteBatch;
        protected GraphicsDeviceManager graphics;

        public GameScreen()
        {
            this.state = ScreenState.Paused;
        }

        public virtual void LoadContent(ContentManager Content)
        {
            spriteBatch = GameServices.GetService<SpriteBatch>();
        }

        public virtual void UnloadContent()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(GameTime gameTime)
        {
        }
    }
}