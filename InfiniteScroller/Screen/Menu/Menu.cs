﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace InfiniteScroller.Screen.Menu
{
    public abstract class Menu : GameScreen
    {
        protected List<MenuItem> menuItems = new List<MenuItem>();
        protected MouseState mState;
        protected Vector2 mousePos;

        /// <summary>
        /// Adds a <i>MenuItem</i> to the menu
        /// </summary>
        /// <param name="item"></param>
        public void AddMenuItem(MenuItem item)
        {
            menuItems.Add(item);
        }

        public override void Update(GameTime gameTime)
        {
            if (state == ScreenState.Off)
            {
                ScreenHandler.RemoveScreen(this);
            }
            UpdateMenuItems(gameTime);
        }

        public void UpdateMenuItems(GameTime gameTime)
        {
            mState = Mouse.GetState();

            mousePos.X = mState.X;
            mousePos.Y = mState.Y;

            foreach (MenuItem menuItem in menuItems)
            {
                Rectangle r = menuItem.getSelectionArea();

                if (r.Contains(mState.X, mState.Y))
                {
                    if (mState.LeftButton == ButtonState.Pressed)
                    {
                        menuItem.onActivation();
                    }
                    else
                    {
                        menuItem.whileSelected();
                    }
                }
                else
                {
                    menuItem.whileNotSelected();
                }

                menuItem.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            foreach (MenuItem menuItem in menuItems)
            {
                menuItem.Draw(spriteBatch);
            }
            spriteBatch.End();
        }
    }
}