﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InfiniteScroller.Screen.Menu
{
    public abstract class MenuItem
    {
        protected Rectangle selectionArea;
        protected bool isSelected;

        public MenuItem(Menu parent, Rectangle selectionArea)
        {
            this.selectionArea = selectionArea;
            isSelected = false;
        }

        public Rectangle getSelectionArea()
        {
            return selectionArea;
        }

        public void OnSelect()
        {
            isSelected = true;
        }

        public void OnDeSelect()
        {
            isSelected = false;
        }

        /// <summary>
        /// Called when the item is moused over/selected, but not pressed
        /// </summary>
        public abstract void whileSelected();

        public abstract void whileNotSelected();

        /// <summary>
        /// Called when the menu item is activated
        /// </summary>
        public abstract void onActivation();

        /// <summary>
        /// The MenuItem's logic. Do not do checking for mouseovers/selections here; that is all handled in Menu's Update()
        /// </summary>
        /// <param name="gameTime">The GameTime variable generically included in all update methods.</param>
        public abstract void Update(GameTime gameTime);

        public abstract void Draw(SpriteBatch spriteBatch);
    }
}