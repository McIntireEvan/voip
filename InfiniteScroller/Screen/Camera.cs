﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InfiniteScroller
{
    internal class Camera
    {
        private Matrix view;
        private Viewport viewport;
        private Vector2 center;

        public Camera(Viewport viewPort)
        {
            this.viewport = viewPort;
            center = new Vector2(viewPort.Width / 2.0f, viewPort.Height / 2.0f);
        }

        public Matrix GetMatrix()
        {
            return view;
        }

        public void Update(GameTime gameTime, Rectangle target)
        {
            view = Matrix.CreateTranslation(new Vector3(-target.Center.X, -490, 0)) *
                                         Matrix.CreateRotationZ(0) *
                                         Matrix.CreateScale(new Vector3(1, 1, 1)) *
                                         Matrix.CreateTranslation(new Vector3(viewport.Width * 0.5f, viewport.Height * 0.5f, 0));
        }
    }
}