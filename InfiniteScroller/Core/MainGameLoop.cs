﻿using InfiniteScroller.Core;
using InfiniteScroller.Screen;
using InfiniteScroller.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InfiniteScroller
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MainGameLoop : Game
    {
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private ScreenHandler screenHandler;
        public bool musicEnabled;
        public Color bgColor;

        public MainGameLoop()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = 689;
            graphics.PreferredBackBufferHeight = 394;
            graphics.ApplyChanges();
            this.Window.Title = "VOIP 0.3 Alpha";
            GameServices.AddService<GraphicsDevice>(GraphicsDevice);
            screenHandler = new ScreenHandler(Content);

            bgColor = new Color(121, 177, 137);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            GameServices.AddService<SpriteBatch>(spriteBatch);
            MainGameScreen menu = new MainGameScreen(this);
            menu.state = ScreenState.Active;
            ScreenHandler.AddScreen(menu);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            ScreenHandler.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(bgColor);

            ScreenHandler.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}