﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace InfiniteScroller
{
    internal class Tile
    {
        private Rectangle rect;
        private Rectangle source;
        private Texture2D sprite;

        public Tile(Rectangle rect, Rectangle source, Texture2D sprite)
        {
            this.rect = rect;
            this.source = source;
            this.sprite = sprite;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, rect, source, Color.White);
        }
    }
}