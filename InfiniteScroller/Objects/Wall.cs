﻿using InfiniteScroller.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InfiniteScroller
{
    internal class Buzzsaw
    {
        private MainGameScreen world;

        private Vector2 pos;
        private AnimatedSprite texture;

        public Buzzsaw(MainGameScreen world, Texture2D texture)
        {
            pos = new Vector2(-400, 0);
            this.texture = new AnimatedSprite(texture, 2, 76, 436);
            this.world = world;
        }

        public Rectangle GetRect()
        {
            return new Rectangle((int)pos.X, 275, 76, 436);
        }

        public void Update(GameTime gameTime)
        {
            pos.X += 5;
            texture.Update(gameTime);
            if (this.GetRect().Intersects(world.player.GetRect()))
            {
                world.GameOver();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            texture.Draw(spriteBatch, this.GetRect());
        }
    }
}