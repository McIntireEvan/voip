﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace InfiniteScroller
{
    internal class Player
    {
        private Vector2 pos;
        private AnimatedSprite running;

        private Texture2D jump;
        private bool isJumping;
        private int jumpTimer;
        KeyboardState previous;

        public Player(Texture2D run, Texture2D jump)
        {
            pos = new Vector2(100, 400);
            running = new AnimatedSprite(run, 6, 150, 181);
            this.jump = jump;
            jumpTimer = 0;
            previous = Keyboard.GetState();
        }

        public Rectangle GetRect()
        {
            return new Rectangle((int)pos.X, (int)pos.Y, 150, 181);
        }

        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                pos.X+=5;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.Space) && !previous.IsKeyDown(Keys.Space))
            {
                isJumping = true;
                pos.Y -= 50;
            }

            if (isJumping)
            {
                jumpTimer += gameTime.ElapsedGameTime.Milliseconds;
                if (jumpTimer > 500)
                {
                    jumpTimer = 0;
                    isJumping = false;
                }
            }

            if (pos.Y != 400)
            {
                pos.Y += 1f;
            }
            previous = Keyboard.GetState();
            running.Update(gameTime);
            pos.X += 3;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (isJumping)
            {
                spriteBatch.Draw(jump, GetRect(), Color.White);
            }
            else
            {
                running.Draw(spriteBatch, GetRect());
            }
        }
    }
}