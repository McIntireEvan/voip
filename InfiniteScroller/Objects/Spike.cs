﻿using InfiniteScroller.Views;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace InfiniteScroller
{
    internal class Spike
    {
        private MainGameScreen world;
        private Player player;
        private Rectangle rect;

        public Spike(MainGameScreen world, Player player, Rectangle rect)
        {
            this.world = world;
            this.player = player;
            this.rect = rect;
        }

        public void Update()
        {
            if (player.GetRect().Intersects(this.rect))
            {
                //world.GameOver();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(world.tiles, rect, new Rectangle(325, 59, 59, 60), Color.White);
        }
    }
}