﻿using InfiniteScroller.Core;
using InfiniteScroller.Screen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Collections.Generic;

namespace InfiniteScroller.Views
{
    internal class MainGameScreen : GameScreen
    {
        private Camera camera;
        public Player player;
        private Buzzsaw wall;
        private MainGameLoop game;
        private Texture2D bg;

        private Song intro;
        private Song loop;
        private bool introDone;

        public Texture2D tiles;

        private List<Spike> spikes;
        private List<Tile> tileList;

        public MainGameScreen(MainGameLoop game)
        {
            this.game = game;
            camera = new Camera(GameServices.GetService<GraphicsDevice>().Viewport);
            spikes = new List<Spike>();
            tileList = new List<Tile>();
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            //Texture2D pixel = Content.Load<Texture2D>("pixel");

            bg = Content.Load<Texture2D>("sprites/bg");
            intro = Content.Load<Song>("music/mainintro");
            loop = Content.Load<Song>("music/mainloop");
            player = new Player(
                Content.Load<Texture2D>("sprites/player/run"),
                Content.Load<Texture2D>("sprites/player/jump")
            );

            //spikes.Add(new Spike(this, player, new Rectangle(400, 490, 60, 60)));

            wall = new Buzzsaw(this, Content.Load<Texture2D>("sprites/buzzsaw"));

            tiles = Content.Load<Texture2D>("sprites/tiles");

            //MediaPlayer.Play(intro);
            introDone = false;

            for (int i = 0; i < 100; i++)
            {
                tileList.Add(new Tile(
                    new Rectangle(0 + (60 * i), 580, 61, 61), 
                    new Rectangle(0, 59, 61, 61),
                    tiles));
                tileList.Add(new Tile(
                    new Rectangle(0 + (60 * i), 640, 61, 61),
                    new Rectangle(0, 190, 61, 61),
                    tiles));
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            camera.Update(gameTime, player.GetRect());
            player.Update(gameTime);
            wall.Update(gameTime);

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                PauseMenu menu = new PauseMenu(game, this);
                menu.state = ScreenState.Active;
                this.state = ScreenState.Paused;
                ScreenHandler.AddScreen(menu);
            }

            if (MediaPlayer.PlayPosition >= intro.Duration && !introDone)
            {
                introDone = true;
                //MediaPlayer.IsRepeating = true;
                //MediaPlayer.Play(loop);
            }

            foreach (Spike s in spikes)
            {
                s.Update();
            }
            
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            spriteBatch.Begin();
            spriteBatch.Draw(bg, new Rectangle(0, 170, 689, 224), Color.White);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, camera.GetMatrix());

            player.Draw(spriteBatch);
            wall.Draw(spriteBatch);

            foreach (Tile t in tileList)
            {
                t.Draw(spriteBatch);
            }

            foreach (Spike s in spikes)
            {
                s.Draw(spriteBatch);
            }

            spriteBatch.End();
        }

        public void GameOver()
        {
            GameOver menu = new GameOver(game);
            menu.state = ScreenState.Active;
            this.state = ScreenState.Off;
            ScreenHandler.AddScreen(menu);
        }
    }
}