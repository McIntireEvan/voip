﻿using InfiniteScroller.Screen;
using InfiniteScroller.Screen.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace InfiniteScroller.Views
{
    internal class PauseMenu : Menu
    {
        private Texture2D screen;

        private Texture2D pointer;
        private int position;

        private KeyboardState previousState;
        private MainGameLoop game;
        private MainGameScreen activeGame;

        public PauseMenu(MainGameLoop game, MainGameScreen activeGame)
        {
            this.game = game;
            this.activeGame = activeGame;
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            screen = Content.Load<Texture2D>("sprites/pause");
            pointer = Content.Load<Texture2D>("sprites/pointer");

            position = 0;
            previousState = Keyboard.GetState();
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Draw(screen, Vector2.Zero, Color.White);
            spriteBatch.Draw(pointer, new Vector2(220, 155 + (45 * position)), Color.White);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();

            if ((state.IsKeyDown(Keys.Down) || state.IsKeyDown(Keys.S)) && (!previousState.IsKeyDown(Keys.Down)) && !previousState.IsKeyDown(Keys.S))
            {
                if (position < 1)
                    position++;
            }
            else if ((state.IsKeyDown(Keys.Up) || state.IsKeyDown(Keys.W)) && (!previousState.IsKeyDown(Keys.Up)) && !previousState.IsKeyDown(Keys.W))
            {
                if (position > 0)
                    position--;
            }
            else if (state.IsKeyDown(Keys.Enter) && !previousState.IsKeyDown(Keys.Enter))
            {
                switch (position)
                {
                    case 0:
                        {
                            activeGame.state = ScreenState.Active;
                            this.state = ScreenState.Off;
                            break;
                        }
                    case 1:
                        {
                            MainMenu menu = new MainMenu(game);
                            menu.state = ScreenState.Active;
                            ScreenHandler.AddScreen(menu);
                            activeGame.state = ScreenState.Off;
                            this.state = ScreenState.Off;
                            break;
                        }
                }
            }
            previousState = Keyboard.GetState();
        }
    }
}