﻿using InfiniteScroller.Screen;
using InfiniteScroller.Screen.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace InfiniteScroller.Views
{
    internal class MainMenu : Menu
    {
        private Texture2D current;
        private Texture2D on;
        private Texture2D off;

        private Texture2D pointer;
        private int position;

        private KeyboardState previousState;
        private MainGameLoop game;

        private Song song;

        public MainMenu(MainGameLoop game)
        {
            this.game = game;
        }

        public override void LoadContent(ContentManager Content)
        {
            base.LoadContent(Content);
            on = Content.Load<Texture2D>("sprites/title-on");
            off = Content.Load<Texture2D>("sprites/title-off");
            pointer = Content.Load<Texture2D>("sprites/pointer");

            song = Content.Load<Song>("music/title");

            current = on;

            position = 0;
            previousState = Keyboard.GetState();
            //MediaPlayer.Play(song);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            spriteBatch.Begin();

            spriteBatch.Draw(current, Vector2.Zero, Color.White);
            spriteBatch.Draw(pointer, new Vector2(240, 275 + (35 * position)), Color.White);

            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();

            if ((state.IsKeyDown(Keys.Down) || state.IsKeyDown(Keys.S)) && (!previousState.IsKeyDown(Keys.Down)) && !previousState.IsKeyDown(Keys.S))
            {
                if (position < 2)
                    position++;
            }
            else if ((state.IsKeyDown(Keys.Up) || state.IsKeyDown(Keys.W)) && (!previousState.IsKeyDown(Keys.Up)) && !previousState.IsKeyDown(Keys.W))
            {
                if (position > 0)
                    position--;
            }
            else if (state.IsKeyDown(Keys.Enter) && !previousState.IsKeyDown(Keys.Enter))
            {
                switch (position)
                {
                    case 0:
                        {
                            MainGameScreen mainGame = new MainGameScreen(game);
                            mainGame.state = ScreenState.Active;
                            ScreenHandler.AddScreen(mainGame);
                            MediaPlayer.Stop();
                            this.state = ScreenState.Off;
                            break;
                        }
                    case 1:
                        {
                            if (MediaPlayer.IsMuted)
                            {
                                current = on;
                                MediaPlayer.IsMuted = false;
                            }
                            else
                            {
                                current = off;
                                MediaPlayer.IsMuted = true;
                            }
                            break;
                        }
                    case 2:
                        {
                            game.Exit();
                            break;
                        }
                }
            }
            previousState = Keyboard.GetState();
        }
    }
}